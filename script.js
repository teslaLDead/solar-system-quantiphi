const canvas = document.getElementById('canvas');
const planetControls = document.querySelector('#orbit-controls ul')
const viewType = document.querySelector('#view-controls input[type="checkbox"]')
let VIEW_2D = false

const planetXOrbitRadius = [90,170.2,240,350.9,480,590.5,660.5,700.1]
const planetRadius = [10,20,25,17,40,35,30,28]

var Mercury = new Planet(90,30,10,"#d1d1d1",0.05)
var Venus = new Planet(170.2,80,20,"#964b05",0.03)
var Earth = new Planet(240,100,25,"#0960bd",0.02)
var Mars = new Planet(350.9,150,17,"#bd2a09",0.01)
var Jupiter = new Planet(480,200,40,"#d19213",0.007)
var Saturn = new Planet(590.5,225,35,"#3c689e",0.004)
var Uranus = new Planet(660.5,250,30,"#3c879e",0.0015)
var Neptune = new Planet(700.1,300.5,28,"#377ebf",0.0005)

viewType.addEventListener('click',()=>{
    VIEW_2D = viewType.checked;
    console.log('yo')
    if(VIEW_2D){
        PlanetArray.forEach(planet => {
            planet.radiusX = planet.radiuxY
            planet.planetSize = 10
        })
    }
    else{
        PlanetArray.forEach((planet,el) => {
            
            planet.radiusX = planetXOrbitRadius[el]
            planet.planetSize = planetRadius[el]
        })
    }
})

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

let C_WIDTH = canvas.width;
let C_HEIGHT = canvas.height;
let CENTER_X = C_WIDTH*0.5
let CENTER_Y = C_HEIGHT*0.5
let starCount = 800
var TIME = 0

const context = canvas.getContext('2d')
const planetContext = canvas.getContext('2d')

// Start object definition
function Star(x,y,r,color){
    this.x = x;
    this.y = y;
    this.r = r;
    this.rChange = 0.05;
    this.color = color;
    this.render = function(){
        context.beginPath();
        context.arc(this.x, this.y, this.r, 0, 2*Math.PI, false);
        context.fillStyle = this.color;
        context.fill();
      }
    this.update = function(){
      
        if (this.r > 2 || this.r < .8){
            this.rChange = - this.rChange;
        }
        this.r += this.rChange;
     }
}




function Planet(radiusX,radiuxY,planetSize,planetColor,orbitalVelocity,orbitSelected){
    this.radiusX = radiusX
    this.radiuxY = radiuxY
    this.planetSize = planetSize
    this.planetColor = planetColor
    this.orbitalVelocity = orbitalVelocity
    this.orbitSelected = orbitSelected

    

    this.render = function () {
        // setup orbital paths
        // Planet path defintion
        context.beginPath();
        context.ellipse(C_WIDTH*0.5, C_HEIGHT*0.5, this.radiusX, this.radiuxY, 0, 0, 2 * Math.PI);
        if (!this.orbitSelected) {
            context.setLineDash([5, 10]);
            context.strokeStyle  = "#bfcfff30";
        }
        else{
            context.setLineDash([5, 5]);
            context.strokeStyle  = "#bfcfff80";
        }
        context.stroke();
        context.beginPath()
        context.arc(CENTER_X-this.radiusX*Math.cos(TIME*this.orbitalVelocity), CENTER_Y-this.radiuxY*Math.sin(TIME*this.orbitalVelocity), this.planetSize, 0, Math.PI * 2, true);
        context.fillStyle = this.planetColor
        context.fill()
    }

    this.selectPath = function(){
        this.orbitSelected = true
    }

}


// selecting random color for stars
function randomColor(){
    var arrColors = ["ffffff30", "ffecd330" , "bfcfff30","ffffff10","e6e7e8"];
    return '#'+arrColors[Math.floor((Math.random()*4))];
}


var arrStars = [];
for(i = 0; i < starCount; i++){
    var randX = Math.floor((Math.random()*C_WIDTH)+1);
    var randY = Math.floor((Math.random()*C_HEIGHT)+1);
    var randR = Math.random() * 0.5 + .5;
    
    var star = new Star(randX, randY, randR, randomColor());
    arrStars.push(star);
}
function updateStars(){
  for(i = 0; i < arrStars.length; i ++){
    arrStars[i].update();
  }
}

// sun 
function drawSun(){
    planetContext.beginPath()
    planetContext.arc(CENTER_X, CENTER_Y, 30, 0, Math.PI * 2, true);
    planetContext.fillStyle ="yellow"
    planetContext.fill()
}





const PlanetArray = [Mercury,Venus,Earth,Mars,Jupiter,Saturn,Uranus,Neptune]

// main animate function
function animate(){
    updateStars();
    TIME+=1
    context.clearRect(0,0,C_WIDTH,C_HEIGHT);
    

    drawSun()

    PlanetArray.forEach(planet => {
        planet.render()
    });

    
   
    for(var i = 0; i < arrStars.length; i++){
      arrStars[i].render();
    }
    requestAnimationFrame(animate);
}

animate();


planetControls.addEventListener('click',(e)=>{
    const planet = e.target
    PlanetArray.forEach(el=>el.orbitSelected=false)
    window[planet.dataset.name].orbitSelected = true
    
})

